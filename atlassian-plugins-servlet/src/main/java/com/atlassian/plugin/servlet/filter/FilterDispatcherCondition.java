package com.atlassian.plugin.servlet.filter;

import javax.servlet.DispatcherType;

/**
 * The dispatching conditions that are taken into account when deciding to match a filter. These match to the dispatcher
 * values allowed in the Servlet API version 2.4.
 *
 * @since 2.5.0
 * @deprecated since 4.6.0. Use {@link javax.servlet.DispatcherType} instead.
 */
@Deprecated
public enum FilterDispatcherCondition {
    REQUEST(DispatcherType.REQUEST),
    INCLUDE(DispatcherType.INCLUDE),
    FORWARD(DispatcherType.FORWARD),
    ERROR(DispatcherType.ERROR),
    /**
     * @since 4.6.0
     */
    ASYNC(DispatcherType.ASYNC);

    private final DispatcherType dispatcherType;

    FilterDispatcherCondition(DispatcherType dispatcherType) {
        this.dispatcherType = dispatcherType;
    }

    /**
     * Determines if a dispatcher value is a valid condition
     *
     * @param dispatcher The dispatcher value. Null allowed.
     * @return True if valid, false otherwise
     */
    public static boolean contains(String dispatcher) {
        for (FilterDispatcherCondition cond : values()) {
            if (cond.toString().equals(dispatcher)) {
                return true;
            }
        }
        return false;
    }

    /**
     * @since 4.6.0
     */
    public DispatcherType toDispatcherType() {
        return dispatcherType;
    }

    /**
     * @since 4.6.0
     */
    public static FilterDispatcherCondition fromDispatcherType(DispatcherType dispatcherType) {
        for (FilterDispatcherCondition cond : values()) {
            if (cond.toDispatcherType().equals(dispatcherType)) {
                return cond;
            }
        }
        return null;
    }
}
