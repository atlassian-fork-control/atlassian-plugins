# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/en/1.0.0/)
and this project adheres to [Semantic Versioning](http://semver.org/spec/v2.0.0.html).

## [5.3.0]

### Added
- Now supports a spring51x bundle, with Spring 5.1.8.RELEASE\_1

## [5.1.0]

### Added
- The `StoredPluginStateAccessor` shows when plugins have been disabled by an administrator and other persistent plugin state.

## [5.0.3]

### Fixed
- Updates Apache Felix to 5.6.12, fixing [FELIX-6035](https://issues.apache.org/jira/browse/FELIX-6035).
  This bug prevented Atlassian Plugins from working with Java 11.0.2, while the 11.0.0 & 11.0.1 versions did
  not experience this problem.

## [5.0.2]
- Reverts the change "Restored autowiring behaviour from pre-5.0.0" made in 5.0.1

## [5.0.1]

### Changed
- Restored autowiring behaviour from pre-5.0.0
- Avoid creation of multiple PluginHttp wrappers around Request and Session objects

### Fixed
- Fixed a regression introduced in 5.0.0 whereby P1 plugins would not load from `$JIRA_HOME/plugins/installed-plugins`

## [5.0.0]

### Changed
- This release adds support for compiling and running `atlassian-plugins` on Java 11
- Upgraded Spring to 5.0. As part of this change, the autowire mode has changed from the deprecated
  auto-detect mode to 'by constructor'. Any components that relied on setter injection will need to annotate
  these setters or fields with `@Inject` or `@Autowired`
- Upgraded Gemini Blueprints to 3.0.0.M01
- Switched from commons-lang to commons-lang3
- Added dependencies to axb, jaxws, javax-annotation and javax-activation, which are no longer provided
  by JDK 11
- Upgraded Mockito
- Updated the plugin transformation pipeline for host components that plugins use without declaring a
  `component-import` for them. Previously, upon encountering the use of a service (say `SomeService`) that
  is provided by the host application, plugin transformation would look up the host component that implements
  this interface and add an OSGI import for _all_ interfaces this component implemented. So, if `SomeServiceImpl`
  implemented both `SomeService` and `FancySomeService` (and exported both interfaces), the OSGI import
  would import both `SomeService` and `FancySomeService`. In addition, OSGI package imports would be added
  for all packages used in the method signatures on `SomeService` and `FancySomeService`.
  
  Starting with 5.0, only the _requested_ interface is considered (`SomeService` in this example), and no OSGI
  imports are added for any additional interfaces that a matching host component also provides, or the packages
  use in their method signatures. 

### Removed
- Removed most methods and types that had been deprecated prior to 1-1-2016. See [api-changelog.md](api-changelog.md)
  for a detailed breakdown of what has been removed and what the replacement functionality is. 

## [4.6.2]

### Changed
- Upgraded Spring 43x bundle to Spring 4.3.20.RELEASE.

## [4.6.1]

### Added
- Now supports a Spring 43x bundle, with Spring 4.3.18.RELEASE and Gemini blueprints 2.1.0.RELEASE.
- Now supports running in Java 11, but *not* compiling against Java 11, when using the Spring 43x bundle.
- Added dependencies on JAXB, javax-annotation and javax-activation jars, as these are no longer provided by JDK 11.

### Changed
- Upgraded Felix framework to 5.6.10
- Improved the exported jdk packages to match Java 8, 9 and 11, so it matches the version we're running in.
- Updated package scanning to use FastClasspathScanner, for compatibility with modern Java platform classloaders.
- Prevents the spring-internal ImportRegistry from being wired as a Collection into any object with a setFoo(Collection).

## [4.6.0]

No changes in this release. Accidentally released from the wrong branch. Do not use.
