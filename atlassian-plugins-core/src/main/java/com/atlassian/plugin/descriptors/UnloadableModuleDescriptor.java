package com.atlassian.plugin.descriptors;

import com.atlassian.plugin.Plugin;
import com.atlassian.plugin.PluginParseException;

/**
 * Instances of this class represent a module which <i>could not be loaded</i>, not a module
 * which <i>can be unloaded</i>.
 */
public final class UnloadableModuleDescriptor extends AbstractNoOpModuleDescriptor<Void> {

    @Override
    protected void loadClass(Plugin plugin, String clazz) throws PluginParseException {
        // don't try to load the class -- we are possibly here because it doesn't exist
    }
}
