package com.atlassian.plugin.jmx;

import com.atlassian.annotations.PublicApi;
import com.atlassian.plugin.PluginController;

/**
 * The JMX interface to plugin manager facilities.
 *
 * @since v3.0.24
 */
@PublicApi
public interface PluginManagerMXBean {
    interface PluginData {
        String getKey();

        String getVersion();

        String getLocation();

        Long getDateLoaded();

        Long getDateInstalled();

        boolean isEnabled();

        boolean isEnabledByDefault();

        boolean isBundledPlugin();
    }

    /**
     * Obtain the currently installed plugins.
     *
     * @return a list of plugins currently installed into the instance.
     */
    PluginData[] getPlugins();

    /**
     * Check each plugin loader for newly available plugins and install them.
     *
     * @return the number of new plugins installed.
     * @see PluginController#scanForNewPlugins()
     */
    int scanForNewPlugins();
}
