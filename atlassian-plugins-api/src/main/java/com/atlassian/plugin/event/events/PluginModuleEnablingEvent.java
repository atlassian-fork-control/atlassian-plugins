package com.atlassian.plugin.event.events;

import com.atlassian.annotations.PublicApi;
import com.atlassian.plugin.ModuleDescriptor;

/**
 * Event fired when a plugin module is about to be enabled, which can also happen when its
 * plugin is about to be enabled or installed.
 *
 * @see com.atlassian.plugin.event.events
 * @since 4.0.0
 */
@PublicApi
public class PluginModuleEnablingEvent extends PluginModuleEvent {
    public PluginModuleEnablingEvent(final ModuleDescriptor<?> module) {
        super(module);
    }
}
