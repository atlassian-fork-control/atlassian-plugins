package com.atlassian.plugin.event.events;

import com.atlassian.annotations.PublicApi;
import com.atlassian.plugin.Plugin;

/**
 * Event that signifies a plugin has been disabled, uninstalled, or updated.
 *
 * @see com.atlassian.plugin.event.events
 * @since 2.1.0
 */
@PublicApi
public class PluginDisabledEvent extends PluginEvent {
    public PluginDisabledEvent(final Plugin plugin) {
        super(plugin);
    }
}
