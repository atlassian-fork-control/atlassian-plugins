package com.atlassian.plugin.event.events;

import com.atlassian.annotations.PublicApi;
import com.atlassian.plugin.ModuleDescriptor;

/**
 * Signifies a plugin module is now unavailable outside the usual installation process.
 *
 * @see com.atlassian.plugin.event.events
 * @since 2.5.0
 */
@PublicApi
public class PluginModuleUnavailableEvent extends PluginModuleEvent {
    public PluginModuleUnavailableEvent(final ModuleDescriptor<?> module) {
        super(module);
    }
}
