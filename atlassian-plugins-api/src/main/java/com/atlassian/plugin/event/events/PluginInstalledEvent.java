package com.atlassian.plugin.event.events;

import com.atlassian.annotations.PublicApi;
import com.atlassian.plugin.Plugin;

/**
 * Event fired after a plugin is installed at runtime.
 *
 * @see com.atlassian.plugin.event.events
 * @since 2.2.0
 */
@PublicApi
public class PluginInstalledEvent extends PluginEvent {
    public PluginInstalledEvent(final Plugin plugin) {
        super(plugin);
    }
}
