package com.atlassian.plugin;

/**
 * Load a snapshot of the stored state for plugins.
 *
 * @since 5.1.0
 */
public interface StoredPluginStateAccessor {
    /**
     * Get the saved activation state of loaded plugins or modules.
     *
     * @return the configured activation/deactivation state for plugins in this instance
     */
    StoredPluginState get();
}
