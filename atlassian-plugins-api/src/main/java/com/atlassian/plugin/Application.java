package com.atlassian.plugin;

/**
 * This represents the application that uses the plugin system.
 *
 * @since 3.0
 */
public interface Application {
    /**
     * The application key, e.g. confluence or jira or fisheye, etc.
     *
     * @return an application key
     */
    String getKey();

    /**
     * The version of the application, as a string. This is for example, 2.1, 3.0.1-beta1, etc.
     *
     * @return the version
     */
    String getVersion();

    /**
     * The build number of the application, as a string. This build number is parseable as an integer.
     * So doing {@code Integer.parseInt(getBuildNumber())} should not thrown any exception.
     *
     * @return the build number
     */
    String getBuildNumber();
}
